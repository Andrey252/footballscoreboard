/**
 * Приложение, имитирующее работу футбольного табло для отображения счета в матче.
 *
 * @author А.В. Рыжкин
 */

package com.example.andrey252.footballscoreboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button team1;
    Button team2;
    Button reset;

    // начальное значение количества очков первой и второй команды
    private int firstTeam = 0;
    private int secondTeam = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        team1 = (Button) findViewById(R.id.first_team);
        team2 = (Button) findViewById(R.id.second_team);
        reset = (Button) findViewById(R.id.reset);
        team1.setOnClickListener(this);
        team2.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("first_team") && savedInstanceState.containsKey("second_team")) {
            firstTeam = savedInstanceState.getInt("first_team");
            secondTeam = savedInstanceState.getInt("second_team");
            outputScoreboard(firstTeam, R.id.txt_score1);
            outputScoreboard(secondTeam, R.id.txt_score2);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("first_team", firstTeam);
        outState.putInt("second_team", secondTeam);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.first_team:
                if (checkAddPoint(firstTeam)) firstTeam++;
                outputScoreboard(firstTeam, R.id.txt_score1);
                break;
            case R.id.second_team:
                if (checkAddPoint(secondTeam)) secondTeam++;
                outputScoreboard(secondTeam, R.id.txt_score2);
                break;
            case R.id.reset:
                onClickResetTeams(view);
                break;
        }
    }

    /**
     * Метод очищает количество очков у обеих команд
     *
     * @param view вьюшка reset
     */
    public void onClickResetTeams(View view) {
        firstTeam = 0;
        secondTeam = 0;
        outputScoreboard(firstTeam, R.id.txt_score1);
        outputScoreboard(secondTeam, R.id.txt_score2);
    }

    /**
     * Метод проверяет количество очков
     * @param team количество очков команды
     * @return возвращает <code>true</code> если количество очков не превышено, иначе <code>false</code>,
     * выводя уведомление пользователю.
     */
    private boolean checkAddPoint(int team) {
        if (team > 98) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    "Превышено количество очков!", Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return true;
    }

    /**
     * Метод выводит количество очков на табло одной из выбранных команд
     *
     * @param team количество очков выбранный команды
     * @param id      идентификатор команды, к которой нужно передать количество очков
     */
    public void outputScoreboard(int team, int id) {
        TextView counterView = (TextView) findViewById(id);
        counterView.setText(String.format(Locale.getDefault(), "%d", team));
    }
}